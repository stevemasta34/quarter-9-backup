package assembler.parser

import assembler.BaseAssemblerTest
import assembler.ImmediateToken
import assembler.TokenType
import assembler.Tokens
import model.rotateLeft
import model.rotateRight
import model.splitEvery
import model.toBinaryString
import kotlin.test.assertEquals
import kotlin.test.assertTrue
import org.junit.Test as test
/**
 * Created by stephen on 12/5/15.
 */
class LogicTest : BaseAssemblerTest() {

    @test fun paddingCheck_BasicTest() {
        val testBinary = "1001"
        val padded = paddingCheck(testBinary, 8)

        val split = padded.splitEvery(4)
        assertEquals("0000", split[0], "Should be padded with leading zeroes")
        assertEquals(testBinary, split[1], "Should be the trailing bits")
        assertTrue { padded.contains(testBinary) }
    }

    @test fun paddingCheck_DuplicateTest() {
        val testBinary = "1001"
        val padded = paddingCheck(testBinary, 4)

        val split = padded.splitEvery(4)
        assertEquals(1, split.size, "Split shouldn't be oversized")
        assertEquals(testBinary, padded, "These should be identical")
    }
    
    @test fun splitImmediate_FourAnd12Test() {
        val binaryString = paddingCheck(Integer.toBinaryString(Integer.parseInt("1FE2", 16)), 16)
        println("String = $binaryString, length = ${binaryString.length}")
        val (fourBits, twelveBits) = splitImmediateString(binaryString, 4, 12)

        assertEquals("0001", fourBits, "I want these bits padded as nibbles")
        assertEquals("111111100010", twelveBits, "These trailing bits should be equal")
    }

    @test fun modifiedConstantCheck_blinkBitTest() {
        val immToken = Tokens.create(TokenType.Immediate, "0x200000") as ImmediateToken
        val hexAsInt = Integer.parseInt("200000", 16)

        assertEquals(expected = hexAsInt, actual = immToken.value, message = "These should be equivalent")

        println("2 as binary = ${2.toBinaryString()}")
        // encode = (encode << 2) | (encode >> 30);
        val rightRotateBinary = (2 rotateRight 1).toBinaryString()
        println("(2 rotateRight 1).toBinaryString() = $rightRotateBinary")
//        println("^^^^ as an Int = ${Integer.parseInt(rightRotateBinary, 2)}")
        var encodeTate = 2 rotateLeft 2 // (2 shl 2) or (2 ushr 30)
        println(" 2 << 2 | 2 >>> 30 = $encodeTate")
        for (r in 0..31 step 2) {
            println(encodeTate)
            encodeTate = encodeTate rotateLeft 2
        }
        for (r in 0..31 step 2) {
            println(encodeTate)
            encodeTate = encodeTate rotateRight 2
        }
        val leftRotateBinary = (2 rotateLeft 2).toBinaryString()
        println("(2 rotateLeft 2).toBinaryString() = $leftRotateBinary")
//        println("^^^^ as an Int = ${Integer.parseInt(leftRotateBinary, 2)}")
    }
}