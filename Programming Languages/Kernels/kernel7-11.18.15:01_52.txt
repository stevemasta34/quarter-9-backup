#
# Note: as of....
# 
# 1:53am, mistake were made with the first ADD command.
# the order of the Rn and Rd bits were reversed. Nothing worked. 
# 2:220am, I've swapped them around. Things still don't work.
# 2:30am, I'm checking the LDR and STR commands for syntax
#
# MOVW R4, 0 - "Zero out" one of the register we want to use
# Cond = 1110 aka "always"
# R4 is 0011, because zero based counting
# 1110 0011 0000 0000 0011 0000 0000 0000 in nibbles
11100011000000000011000000000000
# MOVT R4, 0x3F20 - Add the "array pointer" into the register
# Cond = 1110 aka "always"
# R4 = 0011, destination register
# 0x3F20 is the tophalf of the new word, which must be broken into 1:3 bytes
# Like so: 0011 [register] 1111 0010 0000
# "1110 0011 0100" are the first three nibbles, because documentation says so
# Complete in nibbles 1110 0011 0100 0011 0011 1111 0010 0000
11100011010000110011111100100000
# ADD R2, R4, 0x10 - Add 0x10 to R4 value and store in R2
# Cond = 1110
# Rd = R2 = 0010, destination register
# Rn = R4 = 0100, source register
# imm12 = 0x010, which is a byte(1) wrapped in zeros
# There are 8 bits between the first register and the condition. They are:
# 0010 100(0), where that last "0"
# By nibbles 1110 0010 1000 0100 0010 0000 0001 0000
11100010100000100100000000010000
# LDR R3, (R2) - Writes what R2 points to R3, using an offset to determine R2
# Cond = 1110
# Rt = R3 = 0011, destination register number
# Rn = R0 = 0000, base register 
# Rm = 0010, because I want Register 2. 0000 + 0010 = 0010
# P = 1 # I want pre-indexing
# U = 0 # I don't want to add
# W = 1 # 
# Nibbles: 1110 0100 0011 0000 0011 0000 0000 0010
# 11100100001100000011000000000010
# LDR REWRITE --------------------
# Cond = 1110, Rt = R3 = 0011, Rn = R2, imm12 = 0000 0000 0000
# Nibbles: 1110 0101 0011 0011 0010 0000 0000 0000
11100101001100110010000000000000
# END REWRITE --------------------
# OR R3, R3, 0x200000 # 
# Relatively easy command. We're just making sure R3 has that bit set
# Cond = 1110
# S = 0, because I don't need to set condition bit YET
# Rn is source and Rd is destination register
# Rn = Rd = R3, in this case
# imm12 = 0x200000
# The 3rd to last nibble is the rotational bits, which are doubled.
# The other two nibbles are bits that will be rotated to the RIGHT
# I want 0x200000, which is 0010 0000 0000 0000 0000 0000
# so I can rotate "0000 0010" 6 places: "0011" (because it's doubled)
# Nibbles (should be): 1110 0011 1000 0011 0011 0011 0000 0010
# ----------------------------------------
# ASK FOR HELP HERE: the last three bits don't exactly make sense
# Clarified as of 9:30AM. Words are 4 bytes, nibbles are 4 bits
# The tophalf of a Word is 2 bytes or 4 NIBBLES
# ----------------------------------------
11100011100000110011001100000010
# STR R3, (R2) - immediate ARM
# Cond = 1110
# P = 1, for pre-indexing
# U = 0, don't add
# W = 1, using a 1
# Rn, base register, = 0000, for R0
# Rt, source register, = 0011, for R3
# imm12 = 0000 0000 0010, for +2 to get us to R2 from base register
# Nibbles: 1110 0101 0010 0000 0011 0000 0000 0010
# 11100101001000000011000000000010
# STR REWRITE  --------------------
# Cond = 1110, Rt = R3 = 0011, Rn = R2 = 0010
# Nibbles: 1110 0101 0010 0010 0011 0000 0000 0000
11100101001000100011000000000000
# ENDREWRITE  --------------------
# ADD R3, R4, 0x20
# Cond = 1110
# S = 0, because I don't need condition bits set
# Rn = R4 (been a while) = 0100
# Rd = R3 (not a while) = 0011
# Imm12, modified constant nibbles, so nibble 1/3 is bit-rotate-right-count/2
# The rotation count is doubled. I only need 32, which can fit in two nibbles
# Imm12 = 0000 0010 0000
# Nibbles: 1110 0010 1000 0100 0011 0000 0010 0000
11100010100001000011000000100000
# MOW R2, 0x8000 - page A8-484 Encoding A2
# Cond = 1110
# Imm4 = 0b1000 = 8 of the "0x8000"
# Imm12 = 0b000000000000
# Rd = R2 = 0010
# There are lost of statically set bits here
# Nibbles: 1110 0011 0000 1000 0010 0000 0000 0000
11100011000010000010000000000000
# STR R2, (R3)
# Cond = 1110
# P = 1, for pre-indexing
# U = 0, don't add
# W = 1, using a 1
# Rn, base register, = 0000, for R0
# Rt, source register, = 0010, for R2
# imm12 = 0000 0000 0011, for +3 to get us to R3 from base register
# Nibbles: 1110 0101 0010 0000 0010 0000 0000 0011
# 11100101001000000010000000000011
# STR REWRITE  --------------------
# Cond = 1110, Rt = R2 = 0010, Rn = 0011
# Nibbles = 1110 0101 0010 0011 0010 0000 0000 0000
11100101001000110010000000000000
# ENDREWRITE  --------------------
# B 0xFFFFFE - Branch always -2
# Cond = 1110
# static succeeding bits = 1010
# Imm24 = 1111 1111 1111 1111 1111 1110
# Nibbles: 1110 1010 1111 1111 1111 1111 1111 1110
11101010111111111111111111111110  # This matches Halladays
