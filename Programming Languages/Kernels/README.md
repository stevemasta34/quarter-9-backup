Just to be clear...
-

# File naming convention

- kernel7-{date-form}.img
- {date-form} = {date}:{time-form}
- {date} = MM.DD.YY
- {time-form} = HH_mm


# Other conventions
##### (For the sake of being thorough)

- All images should either be accompanied by a same-named text file
    - this text file should list the changes from the previous version
- This will prevent, forgetful moments
- Also, it is a snapshot-ing methodology

