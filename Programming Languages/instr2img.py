import sys
import re

infile_path = sys.argv[1]
outfile_path = sys.argv[2]

infile = open(infile_path,'r')
#write_bytes = []
outfile = open(outfile_path,'wb')

def get_bytes(line):
    line = re.sub('\s','',line)
    instruction = []
    if line:
        for i in range(0, 31, 8):
            bin_byte = line[i:i+8]
            instruction.append(int(bin_byte,2))
    instruction.reverse() #comment out if byte order is wrong
    #write_bytes.extend(instruction)
    outfile.write(bytes(instruction))

for line in infile:
    no_comments = line.split('#')[0]
    get_bytes(no_comments)
infile.close()

#outfile = open(outfile_path,'wb')
#outfile.write(bytes(write_bytes))
outfile.close()

#3818914306
#11100011 10100000 00000010 00000010 -> e3 a0 02 02
