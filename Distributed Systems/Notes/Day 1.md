11/9/15

Day 1
-

### Prime Factorization

- Leading questions
  - What is a protocol?
  - What is a distributed system?
  - What is Postel's law?

- So what's tough about Prime Factorization?
  - It's hard to tell if a number is prime when it's exceedingly large
  - Prime number are secure, because there s only one way to get to that number

- We're left with trial division

- Question time:
  - How would you build a distributed system of hacking an RSA key?
    - General number field sieve
    - Quadratic modulus

[11:05am]

What is a distributed system?
- Example usages
  - Map->Reduce
  - Redundant data
  - Sharding data
- What about just separation of concerns?
  - "Micro services" is the buzz word
- They wouldn't be sharing memory, but they need to talk to each other
- Sooo then...

### What's protocol?

- Messages that we can use to communicate between each other
  - Instructions that would "make sense", because we know (basically) what to 
  expect

- Syntax vs. Semantic
  - With syntax, you have rules
  - With semantics, you are trying to read meaning

- DOS attack
    - Post /.... HTTP/1.1
    - Host: [Somebody]
    - Content-Length: 1000000000
  - Thus stalling the responder

- Another concern is adoption
  - Somehow, we need to make this easily to switch over
    - A story, SAML adoption
    - "Worm" by Mike Bowden
  - 

- The class overview
  - We have labs, group work, and a final interview.

- Difference between a hash and an encrytion/encoding
  - Hash is one way...

- We're learning as much about distribution design or programming

- REMEBER: optimize in small chunks before scaling out.

### TO-DOs

- (The eagle has landed)[https://lms.neumont.edu/courses/1703055/assignments/8438033]:
  - Root phrase is "hangman style something something"
  - Concurrency APIs should be used
    - Scala is setup out the gate
    - Java
  - the sentence is a five-word sentence: 4_5_4_5_4, where '_' is the space
  - All words are lowercase and within the most common 600 words of English
- Part 2 of TEHL: 
  - POC that the master doesn't know how it's communicating with the slaves, but
  just that it has workers to talk to.
