Student Driven discussion
-


- Optimizations, so things finish more quickly
  1. Split the list of words, and hand out the those to the Nodes
  2. Coverage to randomly check everything

- The assignment is grammatically correct
- What are some more optimizations?
  - For constant time optimizations:
    - 2 for-loops in one direction, 1 in the reverse
    - Offset the 3 for-loops
	-
	
- Something to consider (from Trevor):
  - Aren't the odds of finding the correct words if you choose the:
      - next 5 words
      - vs.
	  - 5 random words
  - Because we are grammatically correct, the linguistic probably space shrinks
  for each inner for-loop
  - Look-up a bi-gram calculator
  


[11:00 AM]

- Some distributed architecture for a multi-node, service sharing system.

[11:10 AM]

- There should be some abstraction for dispatching the work job to the node
    - Lookup: ScalaCL
- We have a beauty of abstraction here, via the interfaces
- Think about the contract between your interfaces.

- Problems with remote work, or interfaced work:
  - Connection Might Drop, so there isn't verification
  - Network Latency
      - Might change the distribution of work completion
  - Network Topology
      - Nodes coming and going from the web of work
  - Slow Nodes
      - How can you tell the difference between slow servers and some node that
      has dropped off?

  - Coordination
      - As an alternative: Workers ask for the work!
      - Semaphore as a metaphor for work completion
	- How does one track progress?
      - A messaging layer seems likely
      - With CloudHaskell, you've got a work generator. Workers just come and
	  get work off the "work queue"

[11:31 AM]

- What is the [Richardson Maturity Model](https://goo.gl/lSJT1S)?
  - [Original](http://www.crummy.com/writing/speaking/2008-QCon/act3.html) 
  - [Overview](http://www.infoq.com/news/2010/03/RESTLevels)
  - [Other](http://martinfowler.com/articles/richardsonMaturityModel.html)
