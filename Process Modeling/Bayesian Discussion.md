## Bayesian Theory
-

- Invented by some reverand

9:05am
- Base format
  - A and B are given events
  - P(A) is the probably of A, and likewise B
  - P(B|A) is probably of B, given A is true
  - P(A|B) = [P(B|A) * P(A)]/P(B)

9:10am
- Monty Hall problem...
  - Steven Ulibarri explains
- Josh says we'll going to get into "believability" a little bit later.

9:15am
- A simple example of using the equation: Cancer probability
  - P(A) = 1%, A = person has cancer
  - P(B) = 2%, B = percentage of people above 65 yrs. old
  - P(B|A) = 5%, definitely has cancer, likelihood of being 65 yrs. old
  - therefore
	- A|B = likelihood of having cancer, given they are 65
	- P(A|B) = (5% * 1%) / 2% = 2.5%
  
9:20am
- Kelvin is asking how these differ from combinations of probabilities
- Josh explains that this model of probability solving factors in more
  information.
- Mr. Cummings thinks that we should discuss factor chaining.
  - This would clear multiple probabilities and the intersections of 
	  event occurrences

9:25am
Ryli's group takes the stand
- Bayesian theory is also called "conditional probability", which means you
continue to acquire information that conditionally changes the probability

- During the process of the Monty Hall problem, as more data is added to the
initial set, the condition changes to your probability of success.
- Bayesian probability looks at history as well, not just statistics

9:40am
Factor chaining is a type of math that performs special multiplication to
include multiple component probabilities.
- To determine some probabilities, we could simulate things.
  - Ex: probability of students acheiving certain grades on an exam
- Factor chaining can be defined as a proportion of some probability to another.
- Another prime example:
  - Radioactive decay.
  - What kind of example (Cummings' question) of this?
    - Ryli: different strengths of radioactive elements interfering with each
    others' decay.

9:55am
Markov chains, models, and probabilities.
- With Markov's math, which states are more likely, as a chain of events
- It is very much like the state transition likelihood from our state machines
from Halladay's "Comp. Theory" class.

10:05am
Some clarity: Markov chains don't care about some overall "state" at all. You
pick some starting location and determine probablities from there.
- This can be used to calculate individual probabilities for the previous
Bayesian equations
- Steve: Google's search probabilities, for link ranking, was done by the chance
of showing up to some given web page.
- Ryli clarifying question: these are basically state machines with weights?
  - Shayne: Yes, they're stateless state machines without you knowing how you
got there.
