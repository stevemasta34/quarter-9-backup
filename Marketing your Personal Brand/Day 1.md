Overview
-

### What we're going to focus on
- Because we all graduate in March, that lines up with Q1 hirings for the year
- How to enter into the work force
- What to talk about in these interviews
- 8:15am
  - Because we are familiar with the interview process, the direction for the 
  sprint is going to much different
  - Don't set yourself up for failure by trying to fit into a culture that isn't
  what you are
- Getting from the College world to the career world


### Discussion
- Be blunt, and let employers know what you are expecting for pay
- Recruiters
  - They are basically real estate agents
- Build a custom resume for the employer that you're interview for, we'll cover
  - How to get through the filter process
  - How to standout, more so, once through the filtering



### To-dos
1) Read the first few chapters of the book
